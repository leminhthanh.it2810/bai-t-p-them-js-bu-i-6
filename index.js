function checkPrime(number) {
    if (number < 2) {
        return false;
    }
    for (var t = 2; t <= Math.sqrt(number); t++) {
        if (number % t == 0)
            return false;
        return true;
    }
}

function printnumber() {
    var result = document.getElementById("result");
    var n = document.getElementById("number-n").value * 1;

    for (var i = 1; i <= n; i++) {
        if (checkPrime(i)) {
            result.innerHTML += `<span>${i}</span>`;
        }
    }
}


